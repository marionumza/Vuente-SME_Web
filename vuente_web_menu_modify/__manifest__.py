{
    'name': "Menu Modify No Update",
    'version': "1.1",
    'author': "Vuente",
    'category': "Tools",
    'summary': "Makes easy to no udpate menus",
    'license':'LGPL-3',
    'demo': [],
    'depends': ['web'],
    'images':[
        'static/description/icon.png',
    ],
    'installable': True,
}