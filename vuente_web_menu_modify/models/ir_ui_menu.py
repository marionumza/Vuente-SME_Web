# -*- coding: utf-8 -*-
import logging
_logger = logging.getLogger(__name__)

from openerp import api, fields, models

class IrUiMenu(models.Model):

    _inherit = "ir.ui.menu"

    ex_noupdate = fields.Boolean(string="No Update", compute="_compute_ex_noupdate")

    @api.one
    def _compute_ex_noupdate(self):
        ex_id = self.env['ir.model.data'].search([('res_id','=', self.id), ('model','=', 'ir.ui.menu')])
        if ex_id:
            self.ex_noupdate = ex_id.noupdate

    @api.one
    def disable_update(self):

        _logger.error("Update External ID")

        ex_id = self.env['ir.model.data'].search([('res_id','=', self.id), ('model','=', 'ir.ui.menu')])[0]
        ex_id.noupdate = True