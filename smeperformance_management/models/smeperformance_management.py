# -*- coding: utf-8 -*-
from odoo import api, fields, models

import requests
import json
import base64
import logging
_logger = logging.getLogger(__name__)

class SMEPerformanceManagement(models.Model):

    _name = 'smeperformance.management'

    description = fields.Text(string="Description")