{
    'name': "SME Performance Management",
    'version': "1.0.0",
    'author': "SME Business Support",
    'category': "Tools",
    'summary': "SME Performance Management",
    'description': "SME Performance Management",
    'license':'LGPL-3',
    'data': [
        
    ],
    'demo': [],
    'images':[
        'static/description/1.jpg',
    ],
    'depends': ['base'],
    'installable': True,
}