==============
MuK Editor Extension
==============

Extends the web editor with a variety of new functions. In
the backend editor you can now select fonts and insert emojis
and symbols. It is also possible to set the editor to fullscreen
mode. In both the front and back ends, you can now add your own
colors to the existing ones using a color picker, as well as embed
videos directly into the page. For this purpose, the existing Media
Dialog has been extended to include a new Video Tab.

Installation
============

To install this module, you need to:

Download the module and add it to your Odoo addons folder. Afterward, log on to
your Odoo server and go to the Apps menu. Trigger the debug modus and update the
list by clicking on the "Update Apps List" link. Now install the module by
clicking on the install button.

Dependencies
=============

This module uses `MoviePy <http://zulko.github.io/moviepy/>`_ and `FFMPEG <https://www.ffmpeg.org/>`_.

MoviePy can be easily installed via pip and depends on the Python modules Numpy,
imageio, Decorator, and tqdm, which will be automatically installed during
MoviePy's installation.

* pip install moviepy

FFMPEG is the multimedia framework, which has to be installed on your system.
Use the following link to download the appropriate software package.

* FFMPEG (https://www.ffmpeg.org/download.html)

Configuration
=============

No additional configuration is needed to use this module.

Usage
=============

Switch to your website and switch to the edit mode. Now four new snippets will
appear in your sidebar.

Credits
=======

Contributors
------------

* Mathias Markl <mathias.markl@mukit.at>

Author & Maintainer
----------

This module is maintained by the `MuK IT GmbH <https://www.mukit.at/>`_.

MuK IT is an Austrian company specialized in customizing and extending Odoo.
We develop custom solutions for your individual needs to help you focus on
your strength and expertise to grow your business.

If you want to get in touch please contact us via mail
(sale@mukit.at) or visit our website (https://mukit.at).
