/**********************************************************************************
* 
*   Copyright (C) 2018 MuK IT GmbH
*    		
*   Odoo Proprietary License v1.0
*   This software and associated files (the "Software") may only be used 
*	(executed, modified, executed after modifications) if you have
*	purchased a valid license from the authors, typically via Odoo Apps,
*	or if you have received a written agreement from the authors of the
*	Software (see the COPYRIGHT file).
*	
*	You may develop Odoo modules that use the Software as a library 
*	(typically by depending on it, importing it and using its resources),
*	but without copying any source code or material from the Software.
*	You may distribute those modules under the license of your choice,
*	provided that this license is compatible with the terms of the Odoo
*	Proprietary License (For example: LGPL, MIT, or proprietary licenses
*	similar to this one).
*	
*	It is forbidden to publish, distribute, sublicense, or sell copies of
*	the Software or modified copies of the Software.
*	
*	The above copyright notice and this permission notice must be included
*	in all copies or substantial portions of the Software.
*	
*	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
*	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
*	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
*	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
*	DEALINGS IN THE SOFTWARE.
*
**********************************************************************************/

odoo.define('muk_web_editor_extension.fields', function (require) {
'use strict';

var ajax = require('web.ajax');
var core = require('web.core');

var summernote_backend = require('web_editor.backend');

var QWeb = core.qweb;
var _t = core._t;

var dom = $.summernote.core.dom;
var range = $.summernote.core.range;
var eventHandler = $.summernote.eventHandler;
var renderer = $.summernote.renderer;
    
summernote_backend.FieldTextHtmlSimple.include({
	_getSummernoteConfig: function () {
		var config = this._super.apply(this, arguments);
		var toolbar = [];
		_.each(config.toolbar, function(element, index, list) {
			if(element[0] === "font") {
				element[1].push('strikethrough');
				element[1].push('superscript');
				element[1].push('subscript');
			}
			if(element[0] === "fontsize") {
				toolbar.push(['fontname', ['fontname']]);
			}
			if(element[0] === "color") {
				toolbar.push(['colorpicker', ['colorpicker']]);
			}
			if(element[0] === "insert") {
				element[1].push('hr');
			}
			if(element[0] === "table") {
				toolbar.push(['misc', ['emoji', 'specialChar']]);
			}
			toolbar.push(element);
		});
		toolbar.push(['fullscreen', ['fullscreen']]);
		config.toolbar = toolbar;
		return config;
	},
});

});