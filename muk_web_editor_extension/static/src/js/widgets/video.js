/**********************************************************************************
* 
*   Copyright (C) 2018 MuK IT GmbH
*    		
*   Odoo Proprietary License v1.0
*   This software and associated files (the "Software") may only be used 
*	(executed, modified, executed after modifications) if you have
*	purchased a valid license from the authors, typically via Odoo Apps,
*	or if you have received a written agreement from the authors of the
*	Software (see the COPYRIGHT file).
*	
*	You may develop Odoo modules that use the Software as a library 
*	(typically by depending on it, importing it and using its resources),
*	but without copying any source code or material from the Software.
*	You may distribute those modules under the license of your choice,
*	provided that this license is compatible with the terms of the Odoo
*	Proprietary License (For example: LGPL, MIT, or proprietary licenses
*	similar to this one).
*	
*	It is forbidden to publish, distribute, sublicense, or sell copies of
*	the Software or modified copies of the Software.
*	
*	The above copyright notice and this permission notice must be included
*	in all copies or substantial portions of the Software.
*	
*	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
*	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
*	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
*	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
*	DEALINGS IN THE SOFTWARE.
*
**********************************************************************************/

odoo.define('muk_web_editor_extension.video', function (require) {
'use strict';

var core = require('web.core');
var editor_widgets = require('web_editor.widget');
var context = require("web_editor.context");

var Widget = require('web.Widget');

var QWeb = core.qweb;
var _t = core._t;

var range = $.summernote.core.range;
var dom = $.summernote.core.dom;

var HtmlVideoDialog = Widget.extend({
	template: 'muk_web_editor_extension.dialog.video',
    xmlDependencies: ['/muk_web_editor_extension/static/src/xml/editor.xml'],
    VIDEOS_PER_ROW: 6,
    VIDEOS_ROWS: 1,
    events: _.extend({}, editor_widgets.Dialog.prototype.events, {
        'change .url-source': function (e) {
            this.changed($(e.target));
        },
        'click button.filepicker': function () {
            var filepicker = this.$('input[type=file]');
            if (!_.isEmpty(filepicker)) {
                filepicker[0].click();
            }
        },
        'click .js_disable_optimization': function () {
            this.$('input[name="disable_optimization"]').val('1');
            var filepicker = this.$('button.filepicker');
            if (!_.isEmpty(filepicker)) {
                filepicker[0].click();
            }
        },
        'change input[type=file]': 'file_selection',
        'submit form': 'form_submit',
        'change input.url': "change_input",
        'keyup input.url': "change_input",
        'click .existing-attachments [data-src]': 'select_existing',
        'dblclick .existing-attachments [data-src]': function () {
            this.getParent().save();
        },
        'click .o_existing_attachment_remove': 'try_remove',
        'keydown.dismiss.bs.modal': function () {},
    }),
	init: function(parent, media, options) {
        this._super.apply(this, arguments);
        this.options = options || {};
        this.accept = this.options.accept || "video/*";
        this.domain = this.options.domain || [['mimetype', 'in', ['video/mp4', 'video/webm', 'video/ogg']]];
        this.parent = parent;
        this.media = media;
        this.page = 0;
    },
    start: function () {
        var self = this;
        this.parent.$(".pager > li").click(function (e) {
            if (!self.$el.is(':visible')) {
                return;
            }
            e.preventDefault();
            var $target = $(e.currentTarget);
            if ($target.hasClass('disabled')) {
                return;
            }
            self.page += $target.hasClass('previous') ? -1 : 1;
            self.display_attachments();
        });
        this.video_options = {
        	autoplay: $(this.media).prop('autoplay') || false,
        	controls: $(this.media).prop('controls') || false,
        	loop: $(this.media).prop('loop') || false,
        	muted: $(this.media).prop('muted') || false,
        	poster: $(this.media).prop('poster') || false,
    	};
        this.$('.settings-attachments #o_html_video_autoplay').change(function() {
        	self.video_options.autoplay =  $(this).prop('checked');
        	self.update_options();
        });
        this.$('.settings-attachments #o_html_video_controls').change(function() {
        	self.video_options.controls =  $(this).prop('checked');
        	self.update_options();
        });
        this.$('.settings-attachments #o_html_video_loop').change(function() {
        	self.video_options.loop =  $(this).prop('checked');
        	self.update_options();
        });
        this.$('.settings-attachments #o_html_video_muted').change(function() {
        	self.video_options.muted =  $(this).prop('checked');
        	self.update_options();
        });
        this.$('.settings-attachments #o_html_video_poster').change(function() {
        	$('.settings-attachments .o_html_video_poster_options').toggle(!!$(this).prop('checked'));
        	if($(this).prop('checked')) {
        		self.video_options.poster = _.str.sprintf('/muk_web_editor/video_poster/%s', self.video.id);
        	} else {
        		self.video_options.poster = false;
        	}
        });
        this.$('.settings-attachments #o_html_video_poster_image').click(function() {
            var $image = $('<img/>');
        	var editor = new editor_widgets.MediaDialog(self, {only_images: true}, $image, $image[0]).open();
        	editor.on('save', this, function () {
        		self.video_options.poster = $image.attr('src');
            	self.update_options();
            });
        });
        this.$('.settings-attachments #o_html_video_poster_options_frame').change(function() {
        	self._rpc({
                route: '/muk_web_editor/create_poster',
                params: {
                	id: self.video.id,
                	second: $(this).val(),
                },
            }).then(function() {
            	self.video_options.poster = _.str.sprintf('/muk_web_editor/video_poster/%s', self.video.id);
            	self.update_options();
            });
        });
        this.fetch_existing().then(function () {
        	var $media = $(self.media);
            if ($media.hasClass('media_video') && $media.data('id')) {
            	var attachment = _.find(self.records, function (record) { 
                	return record.id === $media.data('id');
                });
            	if(attachment) {
                	self.set_video(attachment);
            	}
            }
            self.update_options();
        });
        return this._super.apply(this, arguments);;
    },
    file_selection: function () {
        var $form = this.$('form');
        this.$el.addClass('nosave');
        $form.removeClass('has-error').find('.help-block').empty();
        this.$('button.filepicker').removeClass('btn-danger btn-success');
        $form.submit();
    },
    change_input: function (e) {
        var $input = $(e.target);
        var $button = $input.parent().find("button");
        var emptyValue = ($input.val() === "");
        $button.toggleClass("btn-default", emptyValue).toggleClass("btn-primary", !emptyValue);
    },
    form_submit: function (event) {
        var self = this;
        var $form = this.$('form[action="/web_editor/attachment/add"]');
        if (!$form.find('input[name="upload"]').val().length) {
            if (this.selected_existing().size()) {
                event.preventDefault();
                return false;
            }
        }
        $form.find('.well > div').hide().last().after('<span class="fa fa-spin fa-3x fa-refresh"/>');
        var callback = _.uniqueId('func_');
        this.$('input[name=func]').val(callback);
        window[callback] = function (attachments, error) {
            delete window[callback];
            $form.find('.well > span').remove();
            $form.find('.well > div').show();
            _.each(attachments, function (record) {
            	record.src = record.url || _.str.sprintf('/web/content/%s/%s', record.id, record.name);
            	record.preview = _.str.sprintf('/muk_web_editor/video_preview/%s', record.id);
            });
            if (error || !attachments.length) {
                self.file_selected(null, error || !attachments.length);
            }
            self.video = attachments[0];
            for (var i=0; i<attachments.length; i++) {
                self.file_selected(attachments[i], error);
            }
        };
    },
    file_selected: function (attachment, error) {
        var $button = this.$('button.filepicker');
        if (!error) {
            $button.addClass('btn-success');
            this.set_video(attachment);
        } else {
            this.$('form').addClass('has-error').find('.help-block').text(error);
            $button.addClass('btn-danger');
        }
    },
    set_video: function (attachment) {
        this.push(attachment);
        this.$('input.url').val('');
        this.search();
    },
    push: function (attachment) {
    	this.video = attachment;
    },
    save: function () {
    	var $content = this.$('.o_video_preview video');
        $(this.media).replaceWith($content);
        this.media = $content[0];  
        return this.media;
    },
    clear: function () {
    	if (!this.media) {
            return;
        }
        this.media.className = this.media.className.replace(/(^|\s)media_video(\s|$)/g, ' ');
    },
    fetch_existing: function (name) {
        var domain = [['res_model', '=', 'ir.ui.view']].concat(this.domain);
        if (name && name.length) {
            domain.push('|', ['datas_fname', 'ilike', name], ['name', 'ilike', name]);
        }
        return this._rpc({
            model: 'ir.attachment',
            method: 'search_read',
            args: [],
            kwargs: {
                domain: domain,
                fields: ['name', 'mimetype', 'checksum', 'url', 'type'],
                order: [{name: 'id', asc: false}],
                context: context.get(),
            }
        }).then(this.proxy('fetched_existing'));
    },
    fetched_existing: function (records) {
        this.records = _.uniq(_.filter(records, function (r) {
            return (r.type === "binary" || r.url && r.url.length > 0);
        }), function (r) {
            return (r.url || r.id);
        });
        _.each(this.records, function (record) {
            record.src = record.url || _.str.sprintf('/web/content/%s/%s', record.id, record.name);
        	record.preview = _.str.sprintf('/muk_web_editor/video_preview/%s', record.id);
        });
        this.display_attachments();
    },
    display_attachments: function () {
        var self = this;
        var per_screen = this.VIDEOS_PER_ROW * this.VIDEOS_ROWS;
        var from = this.page * per_screen;
        var records = this.records;
        var rows = _(records).chain()
            .slice(from, from + per_screen)
            .groupBy(function (_, index) { return Math.floor(index / self.VIDEOS_PER_ROW); })
            .values()
            .value();
        this.$('.help-block').empty();
        this.$('.existing-attachments').replaceWith(
        		QWeb.render('muk_web_editor_extension.dialog.video.existing.content', {rows: rows}));
        this.parent.$('.pager')
            .find('li.previous a').toggleClass('disabled', (from === 0)).end()
            .find('li.next a').toggleClass('disabled', (from + per_screen >= records.length));
        this.selected_existing();
    },
    search: function (needle) {
        var self = this;
        this.fetch_existing(needle).then(function () {
            self.selected_existing();
        });
    },
    select_existing: function (e) {
        var $img = $(e.currentTarget);
        var attachment = _.find(this.records, function (record) { 
        	return record.id === $img.data('id'); 
        });
        this.push(attachment);
        this.selected_existing();
    },
    selected_existing: function () {
        var self = this;
        this.$('.o_existing_attachment_cell.o_selected').removeClass("o_selected");
        var $select = this.$('.o_existing_attachment_cell [data-src]').filter(function () {
            var $img = $(this);
            return self.video && self.video.id === $img.data("id");
        });
        $select.closest('.o_existing_attachment_cell').addClass("o_selected");
        this.update_options();
        return $select;
    },
    update_options: function() {
		var $settings = this.$('.settings-attachments');
		var $preview = this.$('.o_video_preview');
		if(this.video) {
			$settings.find('#o_html_video_autoplay').prop('checked', !!this.video_options.autoplay);
			$settings.find('#o_html_video_controls').prop('checked', !!this.video_options.controls);
			$settings.find('#o_html_video_loop').prop('checked', !!this.video_options.loop);
			$settings.find('#o_html_video_muted').prop('checked', !!this.video_options.muted);
			$settings.find('#o_html_video_poster').prop('checked', !!this.video_options.poster);
			$settings.find('.o_html_video_poster_options').toggle(!!this.video_options.poster);
			if(!!this.video_options.poster) {
				$settings.find('.o_poster_preview').empty();
				$settings.find('.o_poster_preview').append(
					$('<img src="' + this.video_options.poster + '" class="img-responsive">')
				);
			}
			var tag =  '<video class="media_video" data-id=' + this.video.id;
			if(!!this.video_options.autoplay) {
				tag += ' autoplay="autoplay"';
			}
			if(!!this.video_options.controls) {
				tag += ' controls="controls"';
			}
			if(!!this.video_options.loop) {
				tag += ' loop="loop"';
			}
			if(!!this.video_options.muted) {
				tag += ' muted="muted"';
			}
			if(!!this.video_options.poster) {
				tag += ' poster="' + this.video_options.poster + '"';
			}
			tag += '>' +
				'<source src="' + this.video.src + '" type="' + this.video.mimetype + '">' +
			'</video>';
    		$preview.empty();
    		$preview.append($(tag));
	        $settings.show();
        } else {
        	$preview.empty();
        	$settings.hide();
        }
    },
    try_remove: function (e) {
        var self = this;
        var $help_block = this.$('.help-block').empty();
        var $a = $(e.target);
        var id = parseInt($a.data('id'), 10);
        var attachment = _.findWhere(this.records, {id: id});
        var $both = $a.parent().children();
        $both.css({borderWidth: "5px", borderColor: "#f00"});
        return this._rpc({
            route: '/web_editor/attachment/remove',
            params: {
                ids: [id],
            },
        }).then(function (prevented) {
        	if (_.isEmpty(prevented)) {
                self.records = _.without(self.records, attachment);
                self.display_attachments();
                if(self.video && self.video.id === id) {
                	self.video = false;
                	self.video_options = {
                    	autoplay: false,
                    	controls: false,
                    	loop: false,
                    	muted: false,
                    	poster: false,
                	};
                }
                self.update_options();
                return;
            }
            $both.css({borderWidth: "", borderColor: ""});
            $help_block.replaceWith(QWeb.render('web_editor.dialog.image.existing.error', {
                views: prevented[id]
            }));
        });
    },
});

editor_widgets.MediaDialog.include({
    start: function () {
    	var self = this;
    	var _super = this._super.apply(this, arguments);
    	this.$("a[href='#editor-media-video']").prepend("Embeded ");
    	this.$("a[href='#editor-media-icon']").parent().after(
    			'<li><a href="#editor-media-html_video" data-toggle="tab">Video</a></li>');
    	this.$("div#editor-media-video").after('<div class="tab-pane fade" id="editor-media-html_video"/>');
    	if(this.only_images) {
            this.$('[href="#editor-media-html_video"]').addClass('hidden');
        }
    	if(this.options.only_videos) {
    		this.$('[href="#editor-media-image"]').addClass('hidden');
    		this.$('[href="#editor-media-document"]').addClass('hidden');
    		this.$('[href="#editor-media-video"]').addClass('hidden');
    		this.$('[href="#editor-media-icon"]').addClass('hidden');
        }
        this.opened((function () {
        	if (this.media) {
            	if(this.media.nodeName === "VIDEO") {
            		this.$('[href="#editor-media-html_video"]').tab('show');
                }
            }
        }).bind(this));
        if(!this.only_images) {
            this.videoHtmlDialog = new HtmlVideoDialog(this, this.media, this.options);
            this.videoHtmlDialog.appendTo(this.$("#editor-media-html_video"));
        }
        this.$('a[data-toggle="tab"]').on('shown.bs.tab', function(event) {
            if($(event.target).is('[href="#editor-media-html_video"]')) {
                self.active = self.videoHtmlDialog;
                self.$('li.search, li.previous, li.next').removeClass("hidden");
            }
        });
        return _super
    },
    save: function() {
        var self = this;
        if(self.media) {
            if (this.videoHtmlDialog && this.active !== this.videoHtmlDialog) {
                this.videoHtmlDialog.clear();
            }
        } 
        this._super.apply(this, arguments);
    },
});

return {
    HtmlVideoDialog: HtmlVideoDialog,
};

});
