/**********************************************************************************
* 
*   Copyright (C) 2018 MuK IT GmbH
*    		
*   Odoo Proprietary License v1.0
*   This software and associated files (the "Software") may only be used 
*	(executed, modified, executed after modifications) if you have
*	purchased a valid license from the authors, typically via Odoo Apps,
*	or if you have received a written agreement from the authors of the
*	Software (see the COPYRIGHT file).
*	
*	You may develop Odoo modules that use the Software as a library 
*	(typically by depending on it, importing it and using its resources),
*	but without copying any source code or material from the Software.
*	You may distribute those modules under the license of your choice,
*	provided that this license is compatible with the terms of the Odoo
*	Proprietary License (For example: LGPL, MIT, or proprietary licenses
*	similar to this one).
*	
*	It is forbidden to publish, distribute, sublicense, or sell copies of
*	the Software or modified copies of the Software.
*	
*	The above copyright notice and this permission notice must be included
*	in all copies or substantial portions of the Software.
*	
*	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
*	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
*	THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
*	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
*	DEALINGS IN THE SOFTWARE.
*
**********************************************************************************/

odoo.define('muk_web_editor_extension.summernote', function (require) {
'use strict';

var ajax = require('web.ajax');
var core = require('web.core');

var QWeb = core.qweb;
var _t = core._t;

var dom = $.summernote.core.dom;
var agent = $.summernote.core.agent;
var range = $.summernote.core.range;
var renderer = $.summernote.renderer;
var options = $.summernote.options;
var eventHandler = $.summernote.eventHandler;
var editor = eventHandler.modules.editor;

var tmpl = $.summernote.renderer.getTemplate();

$.summernote.addPlugin({
    buttons : { 
       "colorpicker": function(lang, options) {
    	   if (typeof(Storage) !== "undefined") {
    		   return tmpl.iconButton(options.iconPrefix + 'tint', {
        		   className: 'btn_colorpicker',
                   event : 'colorpicker',           
                   value : 'ColorPicker',                
                   hide : true
               });   
	   		} 
       }
    }, 
    events : {
       "colorpicker": function(layoutInfo, value) {
    	   var self = this;
    	   var $btn = $(layoutInfo.currentTarget).find('button[data-name="colorpicker"]');
    	   var $colorpicker = $("<input type='text' />");
    	   $btn.after($colorpicker);
    	   $colorpicker.spectrum({
	   		    showInput: true,
			    showInitial: true,
			    showPalette: true,
    		    color: "rgb(152, 0, 0)",
    		    showSelectionPalette: true,
    		    hideAfterPaletteSelect: true,
    		    maxSelectionSize: 7,
    		    preferredFormat: "hex",
    		    cancelText: _t("Cancel"),
    		    chooseText: _t("Select"),
    		    togglePaletteMoreText: _t("Show More"),
    		    togglePaletteLessText: _t("Show Less"),
    		    containerClassName: "muk_colorpicker_palette",
    		    replacerClassName: "muk_colorpicker_replacer",
    		    localStorageKey: "muk_colorpicker",
    		    change: function(color) {
    		        if (localStorage.muk_colorpicker_summernote) {
	    		    	var colors = localStorage.muk_colorpicker_summernote.split(";");
	    		        colors.push(color.toHexString());
	    		        while(colors.length > 7) {
	    		        	colors.shift();
	                    }
	    		        localStorage.muk_colorpicker_summernote = colors.join(";");
    		        } else {
    		        	localStorage.muk_colorpicker_summernote = color.toHexString();
    		        }
    		        $(layoutInfo.currentTarget).trigger("reload_custom_color");
    		    },
    		    hide: function() {
    		    	$colorpicker.spectrum("destroy");
    		    	$colorpicker.remove();
    		    },
    		    palette: [
    		    	[
		        	   "rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
		               "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"
		           ],
		           [
		        	   "rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
		               "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"
		           ], 
		           [
		        	   "rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", 
		               "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", 
		               "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", 
		               "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", 
		               "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", 
		               "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
		               "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
		               "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
		               "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", 
		               "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"
		           ] 
    		    ]
    		});
    	   $colorpicker.spectrum("show");
    	   layoutInfo.stopPropagation();
    	   layoutInfo.preventDefault();
       }
	}     
});

var fn_createPalette = renderer.createPalette;
renderer.createPalette = function ($container, options) {
    fn_createPalette.call(this, $container, options);
    if (localStorage.muk_colorpicker_summernote) {
		var $palette = $container.find(".note-color .note-color-palette");
		$palette.append($("<h6/>", {"text": _t("Custom Colors")}));
    	function addColor(type) {
    		var $type = $container.find(".note-color .note-color-palette[data-target-event='" + type +"']");
            var $row = $("<div/>", {"class": "note-color-row mb8 custom_colors"});
        	var colors = localStorage.muk_colorpicker_summernote.split(";");
        	_.each(colors, function(color, index, list) {
        		$row.append($("<button/>", {
        	    	"class": "note-color-btn",
        	    	"style": "background-color:" + color + ";",
        	    	"data-event": type,
        	    	"data-value": color,
        	    }));
    		});
        	$type.append($row);
    	}
    	$($container).on("reload_custom_color", function(event) {
    		$palette.find(".custom_colors").remove();
    		addColor("backColor");
        	addColor("foreColor");
    	});
    	$container.trigger("reload_custom_color");
    }
};

options.fontSizes = [_t('Default'), 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 20, 22, 24,
	26, 28, 32, 36, 40, 44, 48, 54, 60, 66, 72, 80, 88, 96, 102, 110, 120, 130, 140, 150, 200, 250];

options.fontNames = ['Arial', 'Arial Black', 'Courier', 'Courier New', 'Comic Sans MS', 'Helvetica',
	'Helvetica Neue', 'Helvetica, Arial', 'Impact', 'Lucida Grande', 'Sacramento', 'Roboto', 'Serif', 'Sans'];

var fn_is_img = dom.isImg || function () {};
dom.isImg = function (node) {
    return fn_is_img(node) || (node && node.nodeName === "VIDEO")
};

var fn_attach = eventHandler.attach;
eventHandler.attach = function (oLayoutInfo, options) {
    fn_attach.call(this, oLayoutInfo, options);
    create_dblclick_feature("video", function () {
        eventHandler.modules.imageDialog.show(oLayoutInfo);
    });
    function create_dblclick_feature(selector, callback) {
        var show_tooltip = true;
        oLayoutInfo.editor().on("dblclick", selector, function (e) {
            var $target = $(e.target);
            if (!dom.isContentEditable($target)) {
                return;
            }
            show_tooltip = false;
            callback();
            e.stopImmediatePropagation();
        });
        oLayoutInfo.editor().on("click", selector, function (e) {
            var $target = $(e.target);
            if (!dom.isContentEditable($target)) {
                return;
            }
            show_tooltip = true;
            setTimeout(function () {
                if (!show_tooltip) return;
                $target.tooltip({
                	title: _t('Double-click to edit'),
                	trigger: 'manuel',
                	container: 'body'}).tooltip('show');
                setTimeout(function () {
                    $target.tooltip('destroy');
                }, 800);
            }, 400);
        });
    }
}

});