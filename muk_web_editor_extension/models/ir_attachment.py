# -*- coding: utf-8 -*-

###################################################################################
# 
#    Copyright (C) 2018 MuK IT GmbH
#
#    Odoo Proprietary License v1.0
#    
#    This software and associated files (the "Software") may only be used 
#    (executed, modified, executed after modifications) if you have
#    purchased a valid license from the authors, typically via Odoo Apps,
#    or if you have received a written agreement from the authors of the
#    Software (see the COPYRIGHT file).
#    
#    You may develop Odoo modules that use the Software as a library 
#    (typically by depending on it, importing it and using its resources),
#    but without copying any source code or material from the Software.
#    You may distribute those modules under the license of your choice,
#    provided that this license is compatible with the terms of the Odoo
#    Proprietary License (For example: LGPL, MIT, or proprietary licenses
#    similar to this one).
#    
#    It is forbidden to publish, distribute, sublicense, or sell copies of
#    the Software or modified copies of the Software.
#    
#    The above copyright notice and this permission notice must be included
#    in all copies or substantial portions of the Software.
#    
#    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
#    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
#    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#    DEALINGS IN THE SOFTWARE.
#
###################################################################################

import os
import io
import re
import sys
import base64
import hashlib
import tempfile
import itertools
import logging
import mimetypes
import subprocess

from collections import defaultdict

import requests

from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.exceptions import AccessError, ValidationError
from odoo.tools import config, human_size, ustr, html_escape
from odoo.tools.mimetypes import guess_mimetype

_logger = logging.getLogger(__name__)

try:
    import imageio
except ImportError:
    imageio = False
    _logger.warn('Cannot `import imageio`.')

try:
    from moviepy.editor import VideoFileClip
except ImportError:
    VideoFileClip = False
    _logger.warn('Cannot `import moviepy`.')

class VideoIrAttachment(models.Model):
    
    _inherit = 'ir.attachment'
    
    store_video_poster = fields.Binary(
        string="Video Poster")    
    
    store_video_preview = fields.Binary(
        string="Video Preview")

    def video_preview(self):
        self.ensure_one()
        if self.store_video_preview:
            return self.with_context({}).store_video_preview
        else:
            def generate_gif(data, filename):
                with tempfile.TemporaryDirectory() as tmp_dir:
                    with open(os.path.join(tmp_dir, filename), 'wb') as tmp_output:
                        tmp_output.write(data)
                    clip = VideoFileClip(os.path.join(tmp_dir, filename))
                    try:
                        clip.resize(width=640)
                        frame = 0
                        files = []
                        while clip.duration > frame and frame < 5:
                            filename = os.path.join(tmp_dir, "output_%s.png" % frame)
                            clip.save_frame(filename, t=frame)
                            files.append(filename)
                            frame += 0.25
                    finally:
                        try:
                            clip.reader.close()
                            del clip.reader
                            if clip.audio != None:
                                clip.audio.reader.close_proc()
                                del clip.audio
                            del clip
                        except Exception as e:
                            sys.exc_clear()
                    with imageio.get_writer(os.path.join(tmp_dir, "output.gif"), fps=5, mode='I') as writer:
                        for filename in files:
                            image = imageio.imread(filename)
                            writer.append_data(image)
                    with open(os.path.join(tmp_dir, "output.gif"), 'rb') as tmp_output:
                        preview = base64.b64encode(tmp_output.read())
                        self.store_video_preview = preview
                return preview
            if self.type == "binary" and self.datas_fname:
                return generate_gif(base64.b64decode(self.with_context({}).datas), self.datas_fname)
            elif self.type == "url" and self.url:
                fname = "input.mp4"
                response = requests.get(self.url)
                if 'content-disposition' in response.headers:
                    fname = re.findall("filename=(.+)", response.headers['content-disposition'])
                if 'mimetpye' in response.headers:
                    fname = "input%s" % mimetypes.guess_extension(response.headers['mimetpye'])
                return generate_gif(response.content, fname)
            else:
                return None
    
    def video_poster(self, second):
        def generate_poster(data, filename):
            with tempfile.TemporaryDirectory() as tmp_dir:
                with open(os.path.join(tmp_dir, filename), 'wb') as tmp_output:
                    tmp_output.write(data)
                clip = VideoFileClip(os.path.join(tmp_dir, filename))
                if clip.duration > int(second):
                    try:
                        clip.save_frame(os.path.join(tmp_dir, "output.png"), t=int(second))
                    finally:
                        try:
                            clip.reader.close()
                            del clip.reader
                            if clip.audio != None:
                                clip.audio.reader.close_proc()
                                del clip.audio
                            del clip
                        except Exception as e:
                            sys.exc_clear()
                    with open(os.path.join(tmp_dir, "output.png"), 'rb') as tmp_output:
                        self.store_video_poster = base64.b64encode(tmp_output.read())
        if self.type == "binary" and self.datas_fname:
            generate_poster(base64.b64decode(self.with_context({}).datas), self.datas_fname)
        elif self.type == "url" and self.url:
            fname = "input.mp4"
            response = requests.get(self.url)
            if 'content-disposition' in response.headers:
                fname = re.findall("filename=(.+)", response.headers['content-disposition'])
            if 'mimetpye' in response.headers:
                fname = "input%s" % mimetypes.guess_extension(response.headers['mimetpye'])
            generate_poster(response.content, fname)
    
    @api.multi
    def write(self, vals):
        if 'url' in vals or 'datas' in vals:
            vals['store_video_preview'] = None
        return super(VideoIrAttachment, self).write(vals)