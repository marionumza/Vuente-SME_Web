# -*- coding: utf-8 -*-

###################################################################################
# 
#    Copyright (C) 2018 MuK IT GmbH
#
#    Odoo Proprietary License v1.0
#    
#    This software and associated files (the "Software") may only be used 
#    (executed, modified, executed after modifications) if you have
#    purchased a valid license from the authors, typically via Odoo Apps,
#    or if you have received a written agreement from the authors of the
#    Software (see the COPYRIGHT file).
#    
#    You may develop Odoo modules that use the Software as a library 
#    (typically by depending on it, importing it and using its resources),
#    but without copying any source code or material from the Software.
#    You may distribute those modules under the license of your choice,
#    provided that this license is compatible with the terms of the Odoo
#    Proprietary License (For example: LGPL, MIT, or proprietary licenses
#    similar to this one).
#    
#    It is forbidden to publish, distribute, sublicense, or sell copies of
#    the Software or modified copies of the Software.
#    
#    The above copyright notice and this permission notice must be included
#    in all copies or substantial portions of the Software.
#    
#    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
#    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
#    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#    DEALINGS IN THE SOFTWARE.
#
###################################################################################

import os
import base64
import unittest

from odoo import _
from odoo.tests import common

_path = os.path.dirname(os.path.dirname(__file__))

class AttachmentVideoTestCase(common.TransactionCase):
    
    at_install = False
    post_install = True
    
    def setUp(self):
        super(AttachmentVideoTestCase, self).setUp()
        self.attachment_model = self.env['ir.attachment'].sudo()

    def tearDown(self):
        super(AttachmentVideoTestCase, self).tearDown()
        
    def test_attachment_binary_preview(self):
        with open(os.path.join(_path, 'tests/data/sample.mp4'), 'rb') as file:
            self.sample = self.attachment_model.create({
                'name': "test",
                'type': "binary",
                'datas_fname': "sample.mp4",
                'datas': base64.b64encode(file.read()),
            })
        self.assertTrue(self.sample.video_preview())
    
    def test_attachment_url_preview(self):
        self.sample = self.attachment_model.create({
            'name': "test",
            'type': "url",
            'url': "https://www.w3schools.com/html/mov_bbb.mp4",
        })
        self.assertTrue(self.sample.video_preview())
        
    def test_attachment_binary_poster(self):
        with open(os.path.join(_path, 'tests/data/sample.mp4'), 'rb') as file:
            self.sample = self.attachment_model.create({
                'name': "test",
                'type': "binary",
                'datas_fname': "sample.mp4",
                'datas': base64.b64encode(file.read()),
            })
        self.sample.video_poster(0)
        self.assertTrue(self.sample.store_video_poster)
    
    def test_attachment_url_poster(self):
        self.sample = self.attachment_model.create({
            'name': "test",
            'type': "url",
            'url': "https://www.w3schools.com/html/mov_bbb.mp4",
        })
        self.sample.video_poster(0)
        self.assertTrue(self.sample.store_video_poster)
        

        
    
        